import { NgWizardConfig, NgWizardModule, THEME } from 'ng-wizard';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LowercaseInputDirective } from './directives/lowercase-input.directive';
import { UppercaseInputDirective } from './directives/uppercase-input.directive';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgxMaskModule } from 'ngx-mask';
import { MatDialogModule } from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/template/header/header.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { NavComponent } from './components/template/nav/nav.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { LoginExtComponent } from './views/login-ext/login-ext.component';
import { AlertComponent } from './components/alert/alert.component';
import { DadosUsuarioComponent } from './views/dados-usuario/dados-usuario.component';
import { InformacoesPessoaisComponent } from './views/dados-usuario/informacoes-pessoais/informacoes-pessoais.component';
import { EnderecoComponent } from './views/dados-usuario/endereco/endereco.component';
import { DadosEmpresaComponent } from './views/dados-usuario/dados-empresa/dados-empresa.component';
import { WizardModule } from 'ngx-wizard';
import { VmessageComponent } from './components/vmessage/vmessage.component';
import { RecuperarSenhaComponent } from './views/login-ext/recuperar-senha/recuperar-senha.component';
import { TrocarSenhaComponent } from './views/login-ext/trocar-senha/trocar-senha.component';
import { NotificationComponent } from './components/notification/notification.component';
import { NovoUsuarioComponent } from './views/login-ext/novo-usuario/novo-usuario.component';
import { ProcurarUsuarioComponent } from './views/login-ext/procurar-usuario/procurar-usuario.component';
import { DependentesComponent } from './views/dados-usuario/dependentes/dependentes.component';
import { ModalNovoDependenteComponent } from './views/dados-usuario/dependentes/modal-novo-dependente/modal-novo-dependente.component';
import { ModalTermosDeUsoComponent } from './views/login-ext/novo-usuario/modal-termos-de-uso/modal-termos-de-uso.component';
import { CameraComponent } from './components/camera/camera.component';
import {WebcamModule} from 'ngx-webcam';
import { ModalVisualizarFotoComponent } from './views/dados-usuario/modal-visualizar-foto/modal-visualizar-foto.component';
import { ModalNomeSocialComponent } from './views/dados-usuario/informacoes-pessoais/modal-nome-social/modal-nome-social.component';


const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default,
  lang: { previous: 'Voltar', next: 'Avançar' }
};

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    LoginExtComponent,
    AlertComponent,
    DadosUsuarioComponent,
    InformacoesPessoaisComponent,
    EnderecoComponent,
    DadosEmpresaComponent,
    VmessageComponent,
    LowercaseInputDirective, 
    UppercaseInputDirective,
    RecuperarSenhaComponent, 
    TrocarSenhaComponent, 
    NotificationComponent, 
    NovoUsuarioComponent, 
    ProcurarUsuarioComponent, 
    DependentesComponent, 
    ModalNovoDependenteComponent,
    ModalTermosDeUsoComponent,
    ModalVisualizarFotoComponent,
    CameraComponent,
    ModalNomeSocialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgxMaskModule.forRoot(),
    NgWizardModule.forRoot(ngWizardConfig),
    MatStepperModule,
    MatInputModule,
    WizardModule,
    WebcamModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalNovoDependenteComponent,
    ModalTermosDeUsoComponent,
    ModalVisualizarFotoComponent
  ]
})
export class AppModule { }
