import { NotificationService } from './../components/notification/notification.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private router: Router, 
        private notification: NotificationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            const mensagemError = err.error.mensagem || err.statusText;
            
            if (!err.error.intercecptar) {
                return throwError(err.error);;
            }
            
            this.notification.showError(mensagemError);
            return throwError(mensagemError);
        }))
    }
}