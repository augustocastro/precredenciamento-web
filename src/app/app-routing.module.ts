import { NovoUsuarioComponent } from './views/login-ext/novo-usuario/novo-usuario.component';
import { TrocarSenhaComponent } from './views/login-ext/trocar-senha/trocar-senha.component';
import { RecuperarSenhaComponent } from './views/login-ext/recuperar-senha/recuperar-senha.component';
import { DadosUsuarioComponent } from './views/dados-usuario/dados-usuario.component';
import { LoginExtComponent } from './views/login-ext/login-ext.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcurarUsuarioComponent } from './views/login-ext/procurar-usuario/procurar-usuario.component';

const routes: Routes = [
  { path: '', component: ProcurarUsuarioComponent },
  { path: 'novo-usuario/:cpf', component: NovoUsuarioComponent },
  { path: 'login-ext', component: ProcurarUsuarioComponent },
  { path: 'login-ext/:cpf', component: LoginExtComponent },
  { path: 'dados-usuario', component: DadosUsuarioComponent },
  { path: 'recuperar-senha/:cpf', component: RecuperarSenhaComponent },
  { path: 'trocar-senha', component: TrocarSenhaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
