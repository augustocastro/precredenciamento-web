import { DependenteListagem } from './../model/DependenteListagem';
import { Observable, Subject } from 'rxjs';
import { Dependente } from './../model/Dependente';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DependenteService {
  
  private url = environment.urlBase + '/dependente';

  private dependenteSubject = new Subject<Dependente>();

  constructor(private http: HttpClient) { }

  public salvar(depedente: Dependente, files: Map<String, File>): Observable<Dependente> {
    const formData = new FormData();
    formData.append('dependente', JSON.stringify(depedente));

    files.forEach((value, key) => {
      if (value instanceof File) {
        formData.append(key.toString(), value);
      }
    });

    return this.http.post<Dependente>(`${this.url}`, formData);
  }

  public buscarPorTitular(titular: number): Observable<DependenteListagem> {
    const url = `${this.url}?titular=${titular}`; 
    return this.http.get<DependenteListagem>(url);
  }

  public buscarPorId(id: number): Observable<Dependente> {
    const url = `${this.url}/${id}`; 
    return this.http.get<Dependente>(url);
  }

  public removerPorId(id: number): Observable<void> {
    const url = `${this.url}/${id}`; 
    return this.http.delete<void>(url);
  }

  public sendDependente(depedente: Dependente): void {
    this.dependenteSubject.next(depedente);
  }

  public getDependente(): Observable<Dependente> {
    return this.dependenteSubject.asObservable();
  }
}
