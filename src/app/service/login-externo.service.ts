import { NovaSenhaUsuarioExterno } from './../model/TrocaSenhaUsuarioExterno';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { UsuarioExterno } from '../model/UsuarioExterno';

@Injectable({
  providedIn: 'root'
})
export class LoginExternoService {

  private url = environment.urlBase + '/auth/usuario-externo';

  constructor(private http: HttpClient) { }

  public autenticar(cpf: string, senha: string): Observable<UsuarioExterno> {
    const data = { cpf, senha };
    return this.http.post<UsuarioExterno>(this.url, data);
  }

  public recuperarSenha(cpf: string, email: string): Observable<{}> {
    const data = { cpf, email };
    const url = this.url + '/recuperar-senha';
    return this.http.post(url, data, {observe: 'response'});
  }

  public alterarSenha(novaSenha: NovaSenhaUsuarioExterno): Observable<{}> {
    const url = this.url + `/recuperar-senha/nova-senha/`;
    return this.http.post(url, novaSenha);
  }

  public validarCodigoRecuperacaoSenha(codigo): Observable<{}> {
    const url = this.url + `/recuperar-senha/validar-codigo/?codigo=${codigo}`;
    return this.http.get(url, {observe: 'response'});
  }
}
