import { UsuarioExternoCadastro } from './../model/UsuarioExternoCadastro';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UsuarioExterno } from './../model/UsuarioExterno';
import { Injectable } from '@angular/core';

import { environment } from './../../environments/environment';
import { Arquivo } from '../model/Arquivo';

@Injectable({
  providedIn: 'root'
})
export class UsuarioExternoService {

  private url = environment.urlBase + '/usuario-externo';

  constructor(private http: HttpClient) { }

  
  public salvar(usuario: UsuarioExternoCadastro): Observable<UsuarioExterno> {
    return this.http.post<UsuarioExterno>(this.url, usuario);
  }

  public atualizar(usuario: UsuarioExterno, files: Map<String, File>, anexos: Array<File>): Observable<UsuarioExterno> {
    const formData = new FormData();
    if(anexos != null){
      for(let file of anexos){
        formData.append('anexos', file);
      }
    }
    formData.append('usuario', JSON.stringify(usuario));
    
    files.forEach((value, key) => {
      if (value instanceof File) {
        formData.append(key.toString(), value);
      }
    });

    return this.http.put<UsuarioExterno>(`${this.url}/${usuario.id}`, formData);
  }
  
  public buscarPorCpf(cpf: string): Observable<any> {
    const url = `${this.url}/${cpf}`; 
    return this.http.get(url);
  }

  public buscarPorCpfAndId(id: number, cpf: string): Observable<UsuarioExterno> {
    const url = `${this.url}/?id=${id}&cpf=${cpf}`; 
    return this.http.get<UsuarioExterno>(url);
  }

}
