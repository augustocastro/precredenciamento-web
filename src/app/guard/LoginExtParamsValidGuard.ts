import { Observable } from 'rxjs';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class LoginExtParamsAllowedGuard implements CanActivate {

    constructor(private router: Router) { }

    public canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const allowed = ['recuperar-senha', 'acessar'];
        const param: string = next.paramMap.get('param')!;

        const allow = allowed.includes(param);

        if (!allow) {
            this.router.navigate(['login-ext']);
            return false;
        }
        return true;
    }
}