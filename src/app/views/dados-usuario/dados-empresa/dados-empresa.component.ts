import { UsuarioExterno } from './../../../model/UsuarioExterno';
import { Component, OnInit, Input, AfterViewInit, AfterViewChecked, OnChanges } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dados-empresa',
  templateUrl: './dados-empresa.component.html',
  styleUrls: ['./dados-empresa.component.scss', '../style.scss']
})
export class DadosEmpresaComponent implements OnChanges {

  @Input() 
  form: FormGroup;

  @Input() 
  submitted: boolean;

  @Input() 
  usuario: UsuarioExterno;

  tipoDocEmpregador: string;
  mascaraCampoCnpj: string;

  constructor() { }

  ngOnChanges(): void {
    const tipoDocEmpregador = this.form.get('tipoDocEmpregador').value;
    this.selecionarMascara(tipoDocEmpregador);
  }

  public alterarMascaraDoCampoCnpjDoEmpregador(tipoDocEmpregador): void {
    this.selecionarMascara(tipoDocEmpregador);
    this.form.get('cnpjEmpregador').setValue('');
  }

  private selecionarMascara(tipoDocEmpregador) {
    if (tipoDocEmpregador) {   
      const validators = { 
        cnpj: { mask: '00.000.000/0000-00', validator: /^\d{14}$/}, 
        cei: { mask: '00.000.00000/00', validator: /^\d{12}$/ }, 
        cpf: { mask: '000.000.000-00', validator: /^\d{11}$/ }
      };
      this.mascaraCampoCnpj = validators[tipoDocEmpregador].mask;
      this.tipoDocEmpregador = tipoDocEmpregador;
      this.form.get('cnpjEmpregador').setValidators([Validators.required, Validators.pattern(validators[tipoDocEmpregador].validator)]);
    }
  }

}
