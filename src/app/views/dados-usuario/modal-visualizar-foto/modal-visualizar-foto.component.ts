import { WebcamImage } from 'ngx-webcam';
import { CameraService } from './../../../components/camera/camera.service';

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-modal-visualizar-foto',
  templateUrl: './modal-visualizar-foto.component.html',
  styleUrls: ['./modal-visualizar-foto.component.css', '../style.scss']
})
export class ModalVisualizarFotoComponent implements OnInit, OnDestroy {
  urlImage: any;
  public webcamImage: WebcamImage = null
  public image$;

  constructor(private cameraService: CameraService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  async ngOnInit() {
    this.carregarFoto();
    this.image$ = this.cameraService.getImage().subscribe(value => {
      this.data.form.controls[this.data.propriedade].setValue(value.image); 
      this.carregarFoto();
    });
  }

  ngOnChanges(): void {
    this.carregarFoto();
  }

  private carregarFoto() {
    const { propriedade, form, usuario } = this.data;

    if(form && propriedade && form.controls[propriedade].value instanceof File){
      const reader = new FileReader();
      const file = form.controls[propriedade].value as File;
      reader.readAsDataURL(file); 
      reader.onload = (_event) => { 
        this.urlImage = reader.result; 
        this.webcamImage = new WebcamImage(this.urlImage, file.type, null);
      }
    } else if(usuario && propriedade && usuario[propriedade]) {
      const objectURL = 'data:image/jpeg;base64,' + usuario[propriedade].arquivo;
      this.urlImage = objectURL; 
      this.webcamImage = new WebcamImage(this.urlImage, null, null);
    }
  }
  
  ngOnDestroy(): void {
    this.image$.unsubscribe();
    this.webcamImage = null;
  }

  exibirFoto(){
    console.log("wwdw");
    
  }
}
