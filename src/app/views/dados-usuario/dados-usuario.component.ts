import { UsuarioExternoNomeSocial } from './../../model/UsuarioExternoNomeSocial';
import { Observable, Observer } from 'rxjs';
import { LocalstorageService } from './../../service/localstorage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from './../../components/notification/notification.service';
import { UsuarioExternoService } from './../../service/usuario-externo.service';
import { UsuarioExterno } from './../../model/UsuarioExterno';
import { NgWizardConfig, THEME, TOOLBAR_BUTTON_POSITION } from 'ng-wizard';
import { Component, OnInit, AfterViewInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { EnderecoUsuarioExterno } from 'src/app/model/EnderecoUsuarioExterno';
import { DependentesComponent } from './dependentes/dependentes.component';


@Component({
  selector: 'app-dados-usuario',
  templateUrl: './dados-usuario.component.html',
  styleUrls: ['./dados-usuario.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class DadosUsuarioComponent implements OnInit, AfterViewChecked {

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    cycleSteps: true,
    toolbarSettings: { toolbarButtonPosition: TOOLBAR_BUTTON_POSITION.start }
  };

  informacoesPessoaisForm: FormGroup;
  enderecoForm: FormGroup;
  dadosDaEmpresaForm: FormGroup;
  anexos: FormGroup;
  nomeSocialForm: FormGroup;
  
  submitted = false;
  usuario: UsuarioExterno;

  nomeSocialValido: boolean;

  constructor(
    private formBuilder: FormBuilder, 
    private service: UsuarioExternoService, 
    private notification: NotificationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private localstorageService: LocalstorageService) { 
  }

  ngOnInit() {
    this.criarFormularios();
    this.buscarUsuario();
  }

  ngAfterViewChecked(): void {
    ['informacoesPessoaisForm', 'enderecoForm', 'dadosDaEmpresaForm'].forEach(form => {
      this[form].valueChanges.subscribe(() => {
        const usuario = this.criarUsuarioComValoresDosForms();
        if (usuario.comprovanteNomeSocial) {
          usuario.comprovanteNomeSocial.arquivo = undefined;
        }

        ['fotoPerfil', 'fotoCpf', 'fotoCarteiraTrabalho', 'fotoRG', 'anexos', 'cadastroNomeSocial'].forEach(campo => { 
          delete usuario[campo];
        });

        this.localstorageService.set('usuario', usuario);
      });
    });
  }

  private async buscarUsuario() {
    this.activatedRoute.queryParams.subscribe(params => {
      const cpf = params['cpf'];
      const id = params['id'];
      const usuario: UsuarioExterno = this.localstorageService.get('usuario');
      
      if (usuario && usuario.id == id) {
        this.usuario = usuario;
        this.preencherCampos(this.usuario);
      } else {
        this.service.buscarPorCpfAndId(id, cpf).subscribe(usuario => {
          this.localstorageService.remove('usuario');
          this.usuario = usuario;
          this.preencherCampos(this.usuario);
        }, _ => {
          this.notification.showError('Usuário não encontrado!')
          this.router.navigate(['novo-usuario', '']);
        });
      }
      this.localstorageService.set('titular', id);
    });
  }

  public salvar(): void {
    this.submitted = true;

    const nomeSocialPreenchido = this.informacoesPessoaisForm.get('nomeSocial').value ? true : false;
    const comprovanteNomeSocial = this.informacoesPessoaisForm.get('comprovanteNomeSocial').value || this.usuario.comprovanteNomeSocial ? true : false;
    this.nomeSocialValido = !nomeSocialPreenchido || (nomeSocialPreenchido && this.nomeSocialForm.valid) || (nomeSocialPreenchido && comprovanteNomeSocial);
    
    if (this.informacoesPessoaisForm.valid && this.enderecoForm.valid && this.dadosDaEmpresaForm.valid && this.nomeSocialValido) {
      const usuario = this.criarUsuarioComValoresDosForms();

      ['fotoPerfil', 'fotoCpf', 'fotoRG', 'fotoCarteiraTrabalho', 'anexos', 'comprovanteNomeSocial'].forEach(campo => {
        delete usuario[campo];
      });

      var anexos = this.anexos.get('fileField').value
      const files = new Map<String, File>();
      files.set('fotoPerfil', this.informacoesPessoaisForm.get('fotoPerfil').value);
      files.set('fotoCpf', this.informacoesPessoaisForm.get('fotoCpf').value);
      files.set('fotoRG', this.informacoesPessoaisForm.get('fotoRG').value);
      files.set('fotoCarteiraTrabalho', this.informacoesPessoaisForm.get('fotoCarteiraTrabalho').value);
      files.set('comprovanteNomeSocial', this.informacoesPessoaisForm.get('comprovanteNomeSocial').value);
      
      this.service.atualizar(usuario, files, anexos).subscribe(() => {
        this.notification.showSuccess('Usuário atualizado com sucesso!');
        this.localstorageService.remove('usuario');
        this.submitted = false;
        window.location.reload();
      });
    } else {
      this.notification.showError('Preencha todos os campos corretamente!');
    }
  }

  private preencherCampos(usuario: UsuarioExterno) {
    this.preencherForm(usuario, this.informacoesPessoaisForm);
    this.preencherForm(usuario, this.dadosDaEmpresaForm);
    this.preencherForm(usuario.endereco, this.enderecoForm);
    this.preencherForm(usuario.cadastroNomeSocial, this.nomeSocialForm); 
  }

  private async preencherForm(obj, form) {
    const arquivos = [];
    Object.keys(obj).forEach(campo => {
      if (campo && form.contains(campo)) {
        if (!arquivos.includes(campo)) {
          form.get(campo).setValue(obj[campo]);
        }
      }
    });
  }

  private criarUsuarioComValoresDosForms(): UsuarioExterno {
    let usuario = new UsuarioExterno();
    let endereco = new EnderecoUsuarioExterno();
    let cadastroNomeSocial = new UsuarioExternoNomeSocial();
    
    const informacoesPessoais = this.informacoesPessoaisForm.getRawValue();
    const dadosDaEmpresa = this.dadosDaEmpresaForm.getRawValue();
    const informacoesEndereco = this.enderecoForm.getRawValue();
    const informacoesNomeSocial = this.nomeSocialForm.getRawValue();
    
    usuario = { ...informacoesPessoais, ...dadosDaEmpresa, id: this.usuario.id };
    endereco = { ...informacoesEndereco };
    cadastroNomeSocial = { ...informacoesNomeSocial };

    usuario.endereco = endereco;
    usuario.cadastroNomeSocial = cadastroNomeSocial; 

    return usuario;
  }

  private criarFormularios() {
    this.informacoesPessoaisForm = this.formBuilder.group({
      fotoPerfil: [null, Validators.required],
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      carteiraRegistrada: [true, Validators.required],
      sexo: ['', Validators.required],
      nomeCompleto: ['', Validators.required],
      nomeSocial: [''],
      dataNascimento: ['', Validators.required],
      estadoCivil: ['', Validators.required],
      grauInstrucao: ['', Validators.required],
      rg: ['', Validators.required],
      orgaoEmissor: ['', Validators.required],
      naturalidade: ['', Validators.required],
      nacionalidade: ['', Validators.required],
      profissao: ['', Validators.required],
      numeroCarteiraTrabalho: [''],
      nomePai: [''],
      nomeMae: [''],
      telefoneCelular: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      telefoneResidencial: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      telefoneComercial: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      email: ['', [Validators.email]],
      fotoCpf: [null, Validators.required],
      fotoRG: [null, Validators.required],
      fotoCarteiraTrabalho: [null],
      anexos: [null],
      comprovanteNomeSocial: [null]
    });

    this.enderecoForm = this.formBuilder.group({
      id: [null],
      cep: ['', [Validators.required, Validators.pattern(/^\d{8}$/)]],
      endereco: ['', Validators.required],
      complemento: [''],
      bairro: ['', Validators.required],
      uf: ['', Validators.required],
      cidade: ['', Validators.required],
      habitacao: ['', Validators.required]
    });

    this.dadosDaEmpresaForm = this.formBuilder.group({
      dataAdmisao: ['', Validators.required],
      tipoDocEmpregador: ['', Validators.required],
      cnpjEmpregador: ['', [Validators.required, Validators.pattern(/^\d{14}$/)]],
      nomeEmpresa: ['', Validators.required],
      observacao: ['', Validators.required],
      renda: ['', Validators.required]
    });

    this.anexos = this.formBuilder.group({
      fileField: [null],
    });

    this.nomeSocialForm = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required]],
      rg: ['', Validators.required],
      orgaoEmissorRg: [''],
      dataEmissaoRg: ['', Validators.required],
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      habilitacaoSesc: ['', Validators.required],
      nomeRl: [''],
      rgRl: [''],
      orgaoEmissorRl: [''],
      dataEmissaoRgRl: [''],
      cpfRl: ['', Validators.pattern(/^\d{11}$/)],
      habilitacaoSescRl: [''],
      nomeSocial: ['', Validators.required],
      tipoRequerimento: ['incluir', Validators.required],
    });
  }
}
