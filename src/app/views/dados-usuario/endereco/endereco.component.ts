import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.css', '../style.scss']
})
export class EnderecoComponent implements OnInit {

  @Input() 
  form: FormGroup;

  @Input() 
  submitted: boolean;

  listaHabitacao = [
    { nome: 'PRÓPRIA', valor: 'Própria' },
    { nome: 'ALUGADA', valor: 'Alugada' },
    { nome: 'PENSÃO', valor: 'Pensão' },
    { nome: 'GRATUITA', valor: 'Gratuita' },
  ];

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    
  }

  buscarCep(){
    let cep = this.form.controls['cep'].value.replace(/[^0-9]/, "");;
    if(cep.length == 8){
      return this.http
      .get(`https://viacep.com.br/ws/${cep}/json/`)
      .subscribe(data => this.setValoresCep(data));
    }
  }

  private setValoresCep(response){
    this.form.controls['endereco'].setValue(response.logradouro);
    this.form.controls['bairro'].setValue(response.bairro);
    this.form.controls['uf'].setValue(response.uf);
    this.form.controls['cidade'].setValue(response.localidade);
  }
}
