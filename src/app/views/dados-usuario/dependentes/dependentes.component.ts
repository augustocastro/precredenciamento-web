import { NotificationService } from './../../../components/notification/notification.service';
import { LocalstorageService } from './../../../service/localstorage.service';
import { UsuarioExterno } from './../../../model/UsuarioExterno';
import { DependenteListagem } from './../../../model/DependenteListagem';
import { DependenteService } from './../../../service/dependente.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ModalNovoDependenteComponent } from './modal-novo-dependente/modal-novo-dependente.component';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {Arquivo} from '../../../model/Arquivo'
import { UsuarioExternoService } from 'src/app/service/usuario-externo.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dependentes',
  templateUrl: './dependentes.component.html',
  styleUrls: ['./dependentes.component.scss', '../style.scss']
})
export class DependentesComponent implements OnInit {
  // fileInfos: [] = ;
  public lista_arquivos: Array<File> = new Array<File>();
  public lista_arquivos_editados: Array<Arquivo> = new Array<Arquivo>();
  selectedFiles: FileList;
  filesFormated = [];
  fileField: any;
  public dependentes: DependenteListagem;
  public titular: number;
  usuario: UsuarioExterno;
  @Input() 
  form: FormGroup;

  @Input() 
  submitted: boolean;

  constructor(private dialog: MatDialog, 
    private dependenteService: DependenteService, 
    private localstorageService: LocalstorageService,
    private notificationService: NotificationService,
    private notification: NotificationService,
    private service: UsuarioExternoService,
    private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.titular = this.localstorageService.get('titular');
    this.buscarUsuario()
    this.pesquisarDependentes(this.titular);
    this.dependenteService.getDependente().subscribe(async () => {
      this.pesquisarDependentes(this.titular);
    });
  }

  private async buscarUsuario() {
    this.activatedRoute.queryParams.subscribe(params => {
      const cpf = params['cpf'];
      this.service.buscarPorCpf(cpf).subscribe(usuario => {
        var user = usuario;
        if(user.anexos != null){
          if(user.anexos.length > 0){
            user.anexos.forEach(element => {
              const imageName = element.nome;
              const extensao = element.nome.slice((element.nome.lastIndexOf(".") - 1 >>> 0) + 2);
              const mimetype = this.getMimeType(extensao);
              const imageBlob = this.dataURItoBlob(element.arquivo, mimetype);
              const imageFile = new File([imageBlob], imageName, mimetype);
              this.lista_arquivos.push(imageFile);
            });
          }
        }
      }, _ => {
        this.notification.showError('Usuário não encontrado!')
      });
    });
  }

  getMimeType(file){
    var mimetype;
    switch (file) {
      case "pdf":
        mimetype = { type: 'application/pdf' }
        break;
      case "doc":
        mimetype = { type: 'application/msword' }
        break;
      case "docx":
        mimetype = { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' }
        break;
      case "png":
        mimetype = { type: 'image/png' }
        break;
      case "jpg":
        mimetype = { type: 'image/jpeg' }
        break;
      case "jpeg":
        mimetype = { type: 'image/jpeg' }
        break;
      case "txt":
        mimetype = { type: 'text/plain' }
        break;
      case "pptx":
        mimetype = { type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' }
        break;
      case "pptx":
        mimetype = { type: 'application/vnd.ms-powerpoint' }
        break;
      default:
        break;
      }
      return mimetype 
  }
  
  dataURItoBlob(dataURI, mimetype) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], mimetype);    
    return blob;
 }

  private async pesquisarDependentes(titular: number) {
    const dependentes = await this.dependenteService.buscarPorTitular(titular).toPromise();
    this.dependentes = dependentes;
  }

  public novoDependente() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.data = { titular: this.titular };
    this.dialog.open(ModalNovoDependenteComponent, dialogConfig);
  }

  public editarDependente(dependente) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.data = { titular: this.titular, dependente: dependente };
    this.dialog.open(ModalNovoDependenteComponent, dialogConfig);
  }

  public removerDependente(dependente) {
    this.dependenteService.removerPorId(dependente.id).subscribe(() => {
      this.notificationService.showSuccess('Dependente removido com sucesso!');
      this.dependenteService.sendDependente(dependente);
    });
  }

  selectFiles(event): void {
    this.selectedFiles = event.target.files;
    Array.from(this.selectedFiles).forEach(file => {
      const sizeEmKb = this.humanFileSize(file.size, 'kB');
      const docType = this.verificarExtensao(file.name);
      
      if (parseInt(sizeEmKb) > 200) {
        this.notification.showError('Tamanho do arquivo superior a 200kb');
        return;
      }
      if (!docType) {
        this.notification.showError('Extensão de arquivo não permitida');
        return;
      }
      if(this.lista_arquivos.length >= 20){
        this.notification.showError('Número máximo de arquivos excedidos');
        return;
      }else{
        this.lista_arquivos.push(file);
        this.localstorageService.remove('anexo');
        this.localstorageService.set('anexo', JSON.stringify(this.lista_arquivos));
      }
    });

  }

  public upload():void{
    this.submitted = true;
    for (let file of this.lista_arquivos) {
      this.filesFormated.push(file);
   }
    this.form.controls['fileField'].setValue(this.filesFormated);
    this.notification.showInfo('Arquivo(s) salvo(s)');
  }

  public humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(bytes < thresh) return bytes + ' B';
    var units = si ? ['kB','MB','GB','TB','PB','EB','ZB','YB'] : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(bytes >= thresh);
    return bytes.toFixed(1)+' '+units[u];
  }

  public verificarExtensao(filename){
    let extensao =  filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
    if(extensao == "doc" || extensao == "docx" || extensao == "txt" || extensao == "pdf" || extensao == "ppt" || 
    extensao == "pptx" || extensao == "jpg" || extensao == "png" || extensao == "jpeg"){
      return true;
    }
    return false;
  }
}
