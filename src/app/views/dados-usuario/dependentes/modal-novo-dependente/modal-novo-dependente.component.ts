import { NotificationService } from './../../../../components/notification/notification.service';
import { DependenteService } from './../../../../service/dependente.service';
import { Dependente } from './../../../../model/Dependente';
import { UsuarioExternoService } from './../../../../service/usuario-externo.service';
import { UsuarioExterno } from './../../../../model/UsuarioExterno';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ChangeDetectorRef, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-novo-dependente',
  templateUrl: './modal-novo-dependente.component.html',
  styleUrls: ['./modal-novo-dependente.component.css', '../../style.scss']
})
export class ModalNovoDependenteComponent implements OnInit {

  listaSexo = [
    { nome: 'MASCULINO', valor: 'Masculino' },
    { nome: 'FEMININO', valor: 'Feminino' }
  ];

  listaEstadoCivil = [
    { nome: 'SOLTEIRO', valor: 'Solteiro' },
    { nome: 'CASADO', valor: 'Casado' },
    { nome: 'SEPARADO', valor: 'Separado' },
    { nome: 'DIVORCIADO', valor: 'Divorciado' },
    { nome: 'VIÚVO', valor: 'Viúvo' }
  ];

  listaGrauInstrucao = [
    { nome: 'SEM ESCOLARIDADE', valor: 'Sem escolaridade' },
    { nome: 'ANALFABETO', valor: 'Analfabeto' },
    { nome: 'ALFABETIZADO', valor: 'Alfabetizado' },
    { nome: 'FUNDAMENTAL INCOMPLETO', valor: 'Fundamental incompleto' },
    { nome: 'FUNDAMENTAL COMPLETO', valor: 'Fundamental completo' },
    { nome: 'ENSINO MÉDIO INCOMPLETO', valor: 'Ensino médio incompleto' },
    { nome: 'ENSINO MÉDIO COMPLETO', valor: 'Ensino médio completo' },
    { nome: 'SUPERIOR INCOMPLETO', valor: 'Superior incompleto' },
    { nome: 'SUPERIOR COMPLETO', valor: 'Superior completo' },
  ];

  listaParentesco = [
    { nome: 'ESPOSO(A)', valor: 'ESPOSO(A)' },
    { nome: 'FILHO(A)', valor: 'FILHO(A)' },
    { nome: 'ENTEADO(A)', valor: 'ENTEADO(A)' },
    { nome: 'MAE', valor: 'MAE' },
    { nome: 'PAI', valor: 'PAI' },
    { nome: 'AVO', valor: 'AVO' },
    { nome: 'NETO(A)', valor: 'NETO(A)' },
    { nome: 'PADRASTO/MADRASTA', valor: 'PADRASTO/MADRASTA' },
    { nome: 'COMPANHEIRO(A)', valor: 'COMPANHEIRO(A)' },
    { nome: 'PESSOA SOB GUARDA', valor: 'PESSOA SOB GUARDA' },
  ];

  dependenteForm: FormGroup;
  fotoPerfil: any;
  dependente: Dependente;
  submitted: boolean = false;

  constructor(private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModalNovoDependenteComponent>,
    private notificationService: NotificationService,
    private cd: ChangeDetectorRef,
    private dependenteService: DependenteService,
    @Inject(MAT_DIALOG_DATA) private dados: any) { }
  
  async ngOnInit() {
    this.dependenteForm = this.formBuilder.group({
      fotoPerfil: ['', Validators.required],
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      sexo: ['', Validators.required],
      nomeCompleto: ['', Validators.required],
      nomeSocial: [''],
      dataNascimento: ['', Validators.required],
      estadoCivil: ['', Validators.required],
      parentesco: ['', Validators.required],
      rg: ['', Validators.required], 
      orgaoEmissor: ['', Validators.required],
      naturalidade: ['', Validators.required],
      nacionalidade: ['', Validators.required],
      renda: ['', Validators.required],
      // grauInstrucao: ['', Validators.required],
    });

    if (this.dados.dependente && this.dados.dependente.id) {
      this.dependente = await this.dependenteService.buscarPorId(this.dados.dependente.id).toPromise();
      this.preencherForm(this.dependente, this.dependenteForm);

      if (this.dependente.fotoPerfil && this.dependente.fotoPerfil.arquivo) {
        let objectURL = 'data:image/jpeg;base64,' + this.dependente.fotoPerfil.arquivo;;
        this.fotoPerfil = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        this.dependenteForm.controls['fotoPerfil'].setValidators([]); 
        this.dependenteForm.controls['fotoPerfil'].updateValueAndValidity();
      }
    }
  }

  private preencherForm(obj, form): void {
    const camposImagens = ['fotoPerfil'];
    Object.keys(obj).forEach(campo => {
      if (campo && form.contains(campo) && !camposImagens.includes(campo)) {
        form.get(campo).setValue(obj[campo]);
      }
    });
  }

  public salvar() {
    this.submitted = true;
    if (this.dependenteForm.valid) {
      const dependente = this.criarDependenteComValoresDosForms();
      dependente.titular = this.dados.titular;
      dependente.id = this.dependente ? this.dependente.id : undefined;
      delete dependente.fotoPerfil;
  
      const files = new Map<String, File>();
      files.set('fotoPerfil', this.dependenteForm.get('fotoPerfil').value);
  
      this.dependenteService.salvar(dependente, files).subscribe(() => {
        this.notificationService.showSuccess('Dependente salvo com sucesso!');
        this.dependenteService.sendDependente(dependente);
        this.fechar();
        this.submitted = false;
      });
    } else {
      this.notificationService.showError('Preencha todos os campos corretamente!');
    }
  }

  private criarDependenteComValoresDosForms(): Dependente {
    let dependente = new Dependente();
    dependente = { ...this.dependenteForm.getRawValue() }
    return dependente;
  }

  public fechar() {
    this.dialogRef.close();
  }

  public onFileChange(event, campo) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
    
      reader.onload = (event) => {
        this[campo] = event.target.result;
        this.dependenteForm.controls[campo].setValue(file);
        this.cd.markForCheck();
      };
    }
  }

}
