import { UsuarioExterno } from './../../../../model/UsuarioExterno';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-nome-social',
  templateUrl: './modal-nome-social.component.html',
  styleUrls: ['./modal-nome-social.component.css', '../../style.scss']
})
export class ModalNomeSocialComponent implements OnInit {

  form: FormGroup;
  informacoesPessoaisForm: FormGroup;
  usuario: UsuarioExterno;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<ModalNomeSocialComponent>,
              private cd: ChangeDetectorRef) { }

  concluded = false;

  ngOnInit(): void {
    this.form = this.data.form;
    this.informacoesPessoaisForm = this.data.informacoesPessoaisForm;
    this.usuario = this.data.usuario;
  }



  public fechar() {
    this.dialogRef.close();
  }

  public concluir() {
    console.log(this.form);
    
    this.concluded = true;
    if (this.form.valid || this.informacoesPessoaisForm.get('comprovanteNomeSocial').value || this.usuario.comprovanteNomeSocial) {
      this.dialogRef.close();
    }
  }

  onFileChange(event, campo) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);    
      reader.onload = (event) => {
        this[campo] = event.target.result
        this.informacoesPessoaisForm.controls[campo].setValue(file);     
        this.cd.markForCheck();
      };
    }
  }

  baixarComprovanteNomeSocial() {    
    const extensao = this.usuario.comprovanteNomeSocial.nome.slice((this.usuario.comprovanteNomeSocial.nome.lastIndexOf(".") - 1 >>> 0) + 2);
    const mimetype = this.getMimeType(extensao);
    const blob: Blob = this.dataURItoBlob(this.usuario.comprovanteNomeSocial.arquivo, mimetype);
    
    const url= window.URL.createObjectURL(blob);
    window.open(url);

  }

  getMimeType(file){
    let mimetype;

    switch (file) {
      case "pdf":
        mimetype = { type: 'application/pdf' }
        break;
      case "png":
        mimetype = { type: 'image/png' }
        break;
      case "jpg":
        mimetype = { type: 'image/jpeg' }
        break;
      case "jpeg":
        mimetype = { type: 'image/jpeg' }
        break;
      default:
        break;
    }
    return mimetype 
  }  

  dataURItoBlob(dataURI, mimetype) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], mimetype);    
    return blob;
  }

  downloadRequerimentoNomeSocial() {
    let link = document.createElement("a");
    link.download = "Nome Social.pdf";
    link.href = "assets/Nome Social.pdf";
    link.click();
  }

}
