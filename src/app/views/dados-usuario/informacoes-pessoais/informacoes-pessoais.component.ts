import { ModalNomeSocialComponent } from './modal-nome-social/modal-nome-social.component';
import { CameraService } from './../../../components/camera/camera.service';
import { ActivatedRoute } from '@angular/router';
import { UsuarioExternoService } from './../../../service/usuario-externo.service';
import { UsuarioExterno } from './../../../model/UsuarioExterno';
import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import * as $ from 'jquery';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalVisualizarFotoComponent } from '../modal-visualizar-foto/modal-visualizar-foto.component';

@Component({
  selector: 'app-informacoes-pessoais',
  templateUrl: './informacoes-pessoais.component.html',
  styleUrls: ['./informacoes-pessoais.component.scss', '../style.scss']
})
export class InformacoesPessoaisComponent implements OnInit {

  @Input() 
  form: FormGroup;

  @Input() 
  submitted: boolean;

  @Input() 
  usuario: UsuarioExterno;

  fotoPerfil: any;

  carteiraRegistrada = true;

  @Input()
  nomeSocialForm: FormGroup;

  @Input()
  nomeSocialValido: boolean;

  listaSexo = [
    { nome: 'MASCULINO', valor: 'Masculino' },
    { nome: 'FEMININO', valor: 'Feminino' }
  ];

  listaEstadoCivil = [
    { nome: 'SOLTEIRO', valor: 'Solteiro' },
    { nome: 'CASADO', valor: 'Casado' },
    { nome: 'SEPARADO', valor: 'Separado' },
    { nome: 'DIVORCIADO', valor: 'Divorciado' },
    { nome: 'VIÚVO', valor: 'Viúvo' }
  ];

  listaGrauInstrucao = [
    { nome: 'SEM ESCOLARIDADE', valor: 'Sem escolaridade' },
    { nome: 'ANALFABETO', valor: 'Analfabeto' },
    { nome: 'ALFABETIZADO', valor: 'Alfabetizado' },
    { nome: 'FUNDAMENTAL INCOMPLETO', valor: 'Fundamental incompleto' },
    { nome: 'FUNDAMENTAL COMPLETO', valor: 'Fundamental completo' },
    { nome: 'ENSINO MÉDIO INCOMPLETO', valor: 'Ensino médio incompleto' },
    { nome: 'ENSINO MÉDIO COMPLETO', valor: 'Ensino médio completo' },
    { nome: 'SUPERIOR INCOMPLETO', valor: 'Superior incompleto' },
    { nome: 'SUPERIOR COMPLETO', valor: 'Superior completo' },
  ]

  constructor(private sanitizer: DomSanitizer,
    private service: UsuarioExternoService,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private cameraService: CameraService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
    const cpf = params['cpf'];
    const id = params['id'];

      this.service.buscarPorCpfAndId(id, cpf).subscribe(usuario => {
        this.usuario = usuario;
        if (this.usuario.fotoPerfil && this.usuario.fotoPerfil.arquivo) {
          let objectURL = 'data:image/jpeg;base64,' + this.usuario.fotoPerfil.arquivo;;
          this.fotoPerfil = this.sanitizer.bypassSecurityTrustUrl(objectURL);
          
          this.form.controls['fotoPerfil'].setValidators([]); 
          this.form.controls['fotoPerfil'].updateValueAndValidity();
        }

        if (this.usuario.carteiraRegistrada) {
          this.form.controls['numeroCarteiraTrabalho'].setValidators([Validators.required]);
          this.form.controls['numeroCarteiraTrabalho'].updateValueAndValidity();
        }

        this.atualizarRegraFotoObrigatorio('fotoCarteiraTrabalho', this.form.get('carteiraRegistrada').value);
        this.atualizarRegraFotoObrigatorio('fotoCpf');
        this.atualizarRegraFotoObrigatorio('fotoRG');
      })
    });

    this.cameraService.getImage().subscribe((value) => {
      if (value.field === 'fotoPerfil' && value.field != null) {
        this.renderizarFoto(value.image, 'fotoPerfil');
      }
    });
  }


  onFileChange(event, campo) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      this.renderizarFoto(file, campo);
    }
  }

  private renderizarFoto(file: File, campo: string) {
    const reader = new FileReader();
    reader.readAsDataURL(file);    
    reader.onload = (event) => {
      this[campo] = event.target.result
      this.form.controls[campo].setValue(file);     
      this.cd.markForCheck();
    };
  }

  alterarRegraCampoCarteira() {
    if (this.form.get('carteiraRegistrada').value) {
      this.atualizarRegraFotoObrigatorio('fotoCarteiraTrabalho', true);
      $("label[for='numero-cateira-trabalho']").addClass('obrigatorio');
      this.form.controls['numeroCarteiraTrabalho'].setValidators([Validators.required]);
    } else { 
      this.form.controls['numeroCarteiraTrabalho'].setValidators([]);
      this.atualizarRegraFotoObrigatorio('fotoCarteiraTrabalho', false);
    }
    this.form.controls['numeroCarteiraTrabalho'].updateValueAndValidity();
  }

  atualizarRegraFotoObrigatorio(campo, condicao = true) {
    if (!this.usuario[campo] && condicao) {
      this.form.controls[campo].setValidators(Validators.required); 
    } else {
      this.form.controls[campo].setValidators([]);
    }
    this.form.controls[campo].updateValueAndValidity();
  }

  abrirModalFoto(prop: string){
    this.cameraService.setTipoCampo(prop);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.width = '70%';
    dialogConfig.data = { propriedade: prop, form: this.form, usuario: this.usuario };
    this.dialog.open(ModalVisualizarFotoComponent, dialogConfig);
  }

  abrirModalNomeSocial(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.width = '70%';
    
    this.nomeSocialForm.get('nomeSocial').setValue(this.form.get('nomeSocial').value);
    this.nomeSocialForm.get('nome').setValue(this.form.get('nomeCompleto').value);
    this.nomeSocialForm.get('rg').setValue(this.form.get('rg').value);
    this.nomeSocialForm.get('orgaoEmissorRg').setValue(this.form.get('orgaoEmissor').value);
    this.nomeSocialForm.get('cpf').setValue(this.form.get('cpf').value);

    dialogConfig.data = { form: this.nomeSocialForm, usuario: this.usuario, informacoesPessoaisForm: this.form };
    
    this.dialog.open(ModalNomeSocialComponent, dialogConfig);
  }

}
