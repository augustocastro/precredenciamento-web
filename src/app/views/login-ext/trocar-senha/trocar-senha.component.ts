import { NotificationService } from './../../../components/notification/notification.service';
import { LoginExternoService } from './../../../service/login-externo.service';
import { SenhaValidator } from './senha-validator';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trocar-senha',
  templateUrl: './trocar-senha.component.html',
  styleUrls: ['./trocar-senha.component.css', '../style.scss']
})
export class TrocarSenhaComponent implements OnInit {

  trocarSenhaForm: FormGroup;
  submitted: boolean = false;
  codigo: string;

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private activatedRoute: ActivatedRoute, 
    private loginExternoService: LoginExternoService,
    private notificationService: NotificationService) { }

  async ngOnInit() {
    this.trocarSenhaForm = this.formBuilder.group(
      {
        novaSenha: ['', Validators.required],
        confirmacaoSenha: ['', Validators.required],
      }, 
      { validator: SenhaValidator }
    );
    this.validarCodigo();
  }

  public enviar(): void {
    this.submitted = true;
    if (this.trocarSenhaForm.valid) {
      const { novaSenha, confirmacaoSenha} = this.trocarSenhaForm.getRawValue();
      const data = { novaSenha, confirmacaoSenha, codigo: this.codigo };
      this.loginExternoService.alterarSenha(data).subscribe(_ => {
        this.notificationService.showSuccess('Senha alterada com sucesso!')
        this.redirecionarParaTelaLogin();
      });
    }
  }

  public validarCodigo() {
    this.activatedRoute.queryParams.subscribe(params => {
      const codigo = params['codigo'];
      this.codigo = codigo;

      if (!codigo) {
        this.redirecionarParaTelaLogin();
      }

      this.loginExternoService.validarCodigoRecuperacaoSenha(codigo).subscribe(
        _ => { },
        erro => {
          this.notificationService.showError(erro.mensagem)
          this.redirecionarParaTelaLogin();
        }
      );
    });
  }

  public redirecionarParaTelaLogin() {
    this.router.navigate(['login-ext', '']);
  }

}
