import { ValidatorFn, FormGroup } from "@angular/forms";

export const SenhaValidator: ValidatorFn = (formGroup: FormGroup) => {
    const novaSenha = formGroup.get('novaSenha').value;
    const confirmacaoSenha = formGroup.get('confirmacaoSenha').value;

    if (novaSenha.trim() + confirmacaoSenha.trim()) {
        return novaSenha === confirmacaoSenha ? null : { senhasDiferentes: true };
    } else {
        return null;
    }
}