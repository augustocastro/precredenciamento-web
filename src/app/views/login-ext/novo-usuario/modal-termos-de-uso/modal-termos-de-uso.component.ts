
import { Component, OnInit, Input, ChangeDetectorRef, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioExternoCadastro } from 'src/app/model/UsuarioExternoCadastro';
import { UsuarioExternoService } from 'src/app/service/usuario-externo.service';
import { NovoUsuarioComponent } from '../novo-usuario.component';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'src/app/components/notification/notification.service';

@Component({
  selector: 'app-modal-termos-de-uso',
  templateUrl: './modal-termos-de-uso.component.html',
  styleUrls: ['./modal-termos-de-uso.component.css', '../../style.scss']
})
export class ModalTermosDeUsoComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ModalTermosDeUsoComponent>,
    private usuarioService: UsuarioExternoService,
    private notificationService: NotificationService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) private usuario: any) { }
  
  async ngOnInit() {
  }

  public fechar() {
    this.notificationService.showInfo('Seu cadastro foi Cancelado');
    this.router.navigate(['login-ext']);
    this.dialogRef.close();
  }

  public setUser(){
    const user = this.usuario.usuario;
    this.usuarioService.salvar(user).subscribe(resposta => {
      console.log(resposta);
      this.notificationService.showSuccess('Usuário cadastrado com sucesso!');
      this.dialogRef.close();
      this.router.navigate(['login-ext', user.cpf]);
    });
  }


}
