import { UsuarioExternoService } from './../../../service/usuario-externo.service';
import { UsuarioExternoCadastro } from './../../../model/UsuarioExternoCadastro';
import { NotificationService } from './../../../components/notification/notification.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalTermosDeUsoComponent } from './modal-termos-de-uso/modal-termos-de-uso.component';

@Component({
  selector: 'app-novo-usuario',
  templateUrl: './novo-usuario.component.html',
  styleUrls: ['./novo-usuario.component.css', '../style.scss']
})
export class NovoUsuarioComponent implements OnInit {

  novoUsuarioForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioExternoService,
    private notificationService: NotificationService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.novoUsuarioForm = this.formBuilder.group({
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      nomeCompleto: ['', Validators.required],
      email: ['', [Validators.email]],
      senha: ['', Validators.required],
    });

    const cpf = this.route.snapshot.paramMap.get('cpf');
    this.novoUsuarioForm.get('cpf').setValue(cpf);
  }

  public termosDeUso(usuario){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.width = '500px';
    dialogConfig.disableClose = true;
    dialogConfig.data = { usuario: usuario};
    this.dialog.open(ModalTermosDeUsoComponent, dialogConfig);
  }

  public criar(): void {
    this.submitted = true;
    if (this.novoUsuarioForm.valid) {
      const usuario: UsuarioExternoCadastro = this.novoUsuarioForm.getRawValue();
      console.log(usuario);
      this.termosDeUso(usuario);
    }
  }
}
