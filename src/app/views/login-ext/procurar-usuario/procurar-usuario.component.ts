import { NotificationService } from './../../../components/notification/notification.service';
import { UsuarioExternoService } from './../../../service/usuario-externo.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-procurar-usuario',
  templateUrl: './procurar-usuario.component.html',
  styleUrls: ['./procurar-usuario.component.css', '../style.scss']
})
export class ProcurarUsuarioComponent implements OnInit {

  procurarUsuarioForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder,
    private usuarioExternoService: UsuarioExternoService) 
    { }

  ngOnInit(): void {
    this.procurarUsuarioForm = this.formBuilder.group({
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]]
    });
  }

  public procurar(): void {
    this.submitted = true;
    if (this.procurarUsuarioForm.valid) {
      const cpf = this.procurarUsuarioForm.get('cpf').value;
      this.usuarioExternoService.buscarPorCpf(cpf).subscribe(
      _ => this.router.navigate(['login-ext', cpf]), 
      _ => this.router.navigate(['novo-usuario', cpf])
      );
    }
  }

}
