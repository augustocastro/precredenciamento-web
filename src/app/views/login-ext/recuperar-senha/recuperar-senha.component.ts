import { LoginExternoService } from './../../../service/login-externo.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from './../../../components/notification/notification.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recuperar-senha',
  templateUrl: './recuperar-senha.component.html',
  styleUrls: ['./recuperar-senha.component.css', '../style.scss']
})
export class RecuperarSenhaComponent implements OnInit {

  recuperarSenhaForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder, 
    private notification: NotificationService, 
    private route: ActivatedRoute, 
    private loginExternoService: LoginExternoService) { }

  ngOnInit(): void {
    this.recuperarSenhaForm = this.formBuilder.group({
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      email: ['', [Validators.required, Validators.email]],
    });

    const cpf = this.route.snapshot.paramMap.get('cpf');
    this.recuperarSenhaForm.get('cpf').setValue(cpf);
  }

  public enviar(): void {
    this.submitted = true;
    if (this.recuperarSenhaForm.valid) {
      const { cpf, email } = this.recuperarSenhaForm.getRawValue();
      
      this.loginExternoService.recuperarSenha(cpf, email).subscribe((resposta: Response) => {
        this.submitted = false;  
        const status = resposta.status;
        
        if (status === 200) {
          this.notification.showSuccess(`E-mail enviado para ${email}!`);
          this.recuperarSenhaForm.reset();
        }
      });
    }
  }
}
