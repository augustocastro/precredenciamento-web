import { LoginExternoService } from './../../service/login-externo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login-ext',
  templateUrl: './login-ext.component.html',
  styleUrls: ['./login-ext.component.scss', './style.scss']
})
export class LoginExtComponent implements OnInit {

  loginForm: FormGroup;
  submitted: boolean = false;
  cpf: string;
  
  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute,
    private loginExternoService: LoginExternoService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      cpf: ['', [Validators.required, Validators.pattern(/^\d{11}$/)]],
      senha: ['', Validators.required],
    });

    this.cpf = this.route.snapshot.paramMap.get('cpf');
    this.loginForm.get('cpf').setValue(this.cpf);
  }

  public logar() { 
    this.submitted = true;
    if (this.loginForm.valid) {
      const { cpf, senha } = this.loginForm.getRawValue();
      this.loginExternoService.autenticar(cpf, senha).subscribe(usuario => {
        this.router.navigate(['dados-usuario'], { queryParams: { id: usuario.id, cpf: usuario.cpf } });
      });
    }
  }

  public recuperarSenha() {
    this.router.navigate(['recuperar-senha', this.cpf]);
  }

}
