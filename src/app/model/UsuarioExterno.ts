import { UsuarioExternoNomeSocial } from './UsuarioExternoNomeSocial';
import { Arquivo } from './Arquivo';
import { EnderecoUsuarioExterno } from './EnderecoUsuarioExterno';

export class UsuarioExterno {
    id ?: number;
    cpf: string;
    sexo: string;
    nomeCompleto: string;
    nomeSocial: string;
    dataNascimento: Date;
    estadoCivil: string;
    grauInstrucao: string;
    rg: string;
    orgaoEmissor: string;
    naturalidade: string;
    nacionalidade: string;
    profissao: string;
    numeroCarteiraTrabalho: string;
    nomePai: string;
    nomeMae: string;
    telefoneCelular: string;
    telefoneResidencial: string;
    telefoneComercial: string;
    dataAdmisao: string;
    cnpjEmpregador: string;
    nomeEmpresa: string;
    observacao: string;
    renda: string;
    endereco: EnderecoUsuarioExterno;
    fotoPerfil?: Arquivo;
    fotoCpf?: Arquivo;
    fotoRG?: Arquivo;
    fotoCarteiraTrabalho?: Arquivo;
    tipoDocEmpregador: string;
    carteiraRegistrada: boolean;
    anexo?: Array<Arquivo>;
    cadastroNomeSocial: UsuarioExternoNomeSocial;
    comprovanteNomeSocial: Arquivo;

}