export class DependenteListagem {
    id ?: number;
    cpf: string;
    nomeCompleto: string;
    parentesco: string;
    titular: number;
}