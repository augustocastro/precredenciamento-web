export class UsuarioExternoNomeSocial {
	id ?: number;
	nome: string;
	rg: string;
	orgaoEmissorRg: string;
	dataEmissaoRg: Date;
	cpf: string;
	habilitacaoSesc: string;
	nomeRl: string;
	rgRl: string;
	orgaoEmissorRl: string;
	dataEmissaoRgRl: Date;
	cpfRl: string;
	habilitacaoSescRl: string;
	nomeSocial: string;
	tipoRequerimento: string;
}