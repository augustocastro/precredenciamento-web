export class EnderecoUsuarioExterno {
    id ?: number;
    cep: string;
    endereco: string;
    complemento: string;
    bairro: string;
    uf: string;
    cidade: string;
    habitacao: string;
}