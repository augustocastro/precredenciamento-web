import { Arquivo } from './Arquivo';

export class Dependente {
    id ?: number;
    cpf: string;
    sexo: string;
    nomeCompleto: string;
    nomeSocial: string;
    dataNascimento: Date;
    parentesco: string;
    estadoCivil: string;
    rg: string;
    orgaoEmissor: string;
    naturalidade: string;
    nacionalidade: string;
    renda: string;
    // grauInstrucao: string;
    fotoPerfil?: Arquivo;
    titular: number;
}