import { Component, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-alert',
  template: `
    <div #alert class="alert alert-dismissible" role="alert">
    <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
      <ng-content></ng-content>
    </div>
  `,
})

export class AlertComponent implements AfterViewInit {

  @Input('type-alert') typeAlert: string;
  @ViewChild('alert') el:ElementRef;

  constructor() { }
  
  ngAfterViewInit(): void {
    $(this.el.nativeElement).addClass(this.typeAlert);
    $(this.el.nativeElement).append();

    $('.close').click(function() {
      $(".alert").css("display", "none");
    });
  }

}
