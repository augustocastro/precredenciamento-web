import { NotificationComponent } from './notification.component';
import { Injectable } from '@angular/core';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  configSuccess: MatSnackBarConfig = {
      panelClass: 'style-success',
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    };

constructor(public snackBar: MatSnackBar) { }

showInfo(message: string): void {
  this.snackBar.openFromComponent(NotificationComponent, {
      data: {
          message:message,
          icon:"fas fa-info-circle",
          style:"info"
      },
      ...this.configSuccess
    });
}

showSuccess(message: string): void {
  this.snackBar.openFromComponent(NotificationComponent, {
      data: {
          message:message,
          icon:"fas fa-check",
          style:"success"
      },
      ...this.configSuccess
    });
}

showWarning(message: string): void {
  this.snackBar.openFromComponent(NotificationComponent, {
      data: {
          message:message,
          icon:"fas fa-exclamation-triangle",
          style:"warning"
      },
      ...this.configSuccess
    });
}


showError(message: string): void {
  this.snackBar.openFromComponent(NotificationComponent, {
      data: {
          message:message,
          icon:"fas fa-ban",
          style:"danger"
      },
      ...this.configSuccess
    });
}
}