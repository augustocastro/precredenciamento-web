import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CameraService {
  public nomeCampo = new BehaviorSubject<any>([]);
  private trigger: Subject<any> = new Subject<any>();
  cast = this.nomeCampo.asObservable();
  
  constructor() { }


  public sendImage(image: File, field: string): void {
    this.trigger.next({ image, field });
  }

  setTipoCampo(ementa) {
    this.nomeCampo.next(ementa);
  }

  public getImage(): Observable<any> {
    return this.trigger.asObservable();
  }

}
