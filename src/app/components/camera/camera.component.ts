import { CameraService } from './camera.service';
import { Subject, Observable, Observer } from 'rxjs';
import { WebcamImage } from 'ngx-webcam';
import { Component, OnInit, Input, ChangeDetectorRef, Output, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit, OnDestroy {
  nomeCampo;

  @Output() fotoPerfil

  @Input()
  public webcamImage: WebcamImage;

  @Input()
  public propriedade: string;

  private trigger: Subject<void> = new Subject<void>();
  private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();

  constructor(private cameraService: CameraService,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.cameraService.cast.subscribe(nome => this.nomeCampo = nome);
  }

  ngOnDestroy(): void {
    this.webcamImage = null;
  }

  triggerSnapshot(): void {
    this.trigger.next();
  }
  
  async handleImage(webcamImage: WebcamImage) {
    const blob: Blob = await this.dataURIToBlob(webcamImage.imageAsBase64).toPromise();
    const file = new File([blob], `${new Date().getTime()}.jpeg`, { type: "image/jpeg" });
    this.cameraService.sendImage(file, this.propriedade);    
    this.webcamImage = webcamImage;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  dataURIToBlob(dataURI: string): Observable<Blob> {
    return Observable.create((observer: Observer<Blob>) => {
      const byteString: string = window.atob(dataURI);
      const arrayBuffer: ArrayBuffer = new ArrayBuffer(byteString.length);
      const int8Array: Uint8Array = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([int8Array], { type: "image/jpeg" });
      observer.next(blob);
      observer.complete();
    });
  }

  public onFileChange(event, campo) {
    
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (event) => {
        this[campo] = event.target.result;
        this.cameraService.sendImage(file, this.propriedade);  
      };
    }
  }

  public tirarOutraFoto(){
    this.webcamImage = null;
  }

}
